package algorithm;
/*
This is Euclid's Algorithm to calculate GCD of two positive numbers.

 */
public class EuclidsAlgorithm {

    public static void main(String[] args) {
        int m = 36;
        int n = 8;
        int gcd = findGcd(m,n);
        System.out.println("GCD is " + gcd);
    }

    public static int findGcd(int m,int n){

        if(m<n)return -1;

        int remainder = m%n;
        if(remainder ==0) return n;

        return  findGcd(n,remainder);

    }
}
