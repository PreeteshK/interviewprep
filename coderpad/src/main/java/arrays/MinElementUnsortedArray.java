package arrays;

public class MinElementUnsortedArray {

    public static void main(String[] args) {

        int arr[] = {64,25,12,22,11,5,2};

        int minIndex = 0;

        for (int i = 1; i <arr.length ; i++) {
                if (arr[i] < arr[minIndex])
                    minIndex = i;
        }

        System.out.println("The smallest element of the array is " + arr[minIndex]);
    }
}
