package collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LinkedHashMapTest {

    public static void main(String[] args) {

        Map<String, String> lhm =
                new LinkedHashMap<>();
        lhm.put("one", "practice.geeksforgeeks.org");
        lhm.put("two", "code.geeksforgeeks.org");
        lhm.put("four", "quiz.geeksforgeeks.org");

        System.out.println(lhm);
        Map map = new HashMap(lhm);
        System.out.println(map);
    }
}
