package codepadQuestions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class FirstNonRepeatingCharacter {


    public static void main(String args[])
    {

        String[] inputs = {"apple","racecars", "ababdc","aaaaaaaaaaaa", null};
        char[] outputs = {'a', 'e', 'd','\0','\0' };

        boolean result = true;
        for(int i = 0; i < inputs.length; i++ )
        {
            result = result && findFirst(inputs[i]) == outputs[i];
            if(!result)
                System.out.println("Test failed for: " + inputs[i]);
            else
                System.out.println("Test passed for: " + inputs[i]);
        }
    }



    public static char findFirst(String input)
    {
        // code goes here
        // return( 'a' );
        if(input == null)return '\0';

        Map<Character,Integer> charCountMap = new LinkedHashMap<Character,Integer>();

        for(int i = 0;i<input.length();i++){

            Character ch = input.charAt(i);
            if(charCountMap.containsKey(ch)){
                charCountMap.put(ch,charCountMap.get(ch)+1);
            }else{
                charCountMap.put(ch,1);
            }
        }

        //  System.out.println(charCountMap);
        // return 'a';

        //Map.Entry<Character,Integer> entry = charCountMap.entrySet();

       //  char charOutput = '\0';

 Optional<Character> op = charCountMap.entrySet().stream().filter(e -> (e.getValue() ==1)).map(e->e.getKey()).findFirst();
      //  System.out.println("Output is +++" + op.get());
          /* if(op != null || op.get() != null) return op.get();
           else return '\0';
*/

          if(op.isPresent())return op.get();
          else return '\0';
/*
     for (Map.Entry<Character,Integer> entry : charCountMap.entrySet()){

       if(entry.getValue() == 1) {

         charOutput = entry.getKey();
         break;
   }
     }
*/

      //  return charOutput;
    }
}
