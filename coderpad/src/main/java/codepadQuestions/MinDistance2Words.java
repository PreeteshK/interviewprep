package codepadQuestions;

public class MinDistance2Words {

    public static void main(String[] args) {
        if (pass()) {
            System.out.println("Pass");
        } else {
            System.out.println("Some Fail");
        }
    }

    private static final String document;
    static{
        StringBuffer sb = new StringBuffer();
        sb.append("In publishing and graphic design, lorem ipsum is a filler text commonly used to demonstrate the graphic elements");
        sb.append(" lorem ipsum text has been used in typesetting since the 1960s or earlier, when it was popularized by advertisements");
        sb.append(" for Letraset transfer sheets. It was introduced to the Information Age in the mid-1980s by Aldus Corporation, which");

        document = sb.toString();
    }

    public static boolean pass() {
        return  shortestDistance(document, "and", "graphic") == 6d &&
                shortestDistance(document, "transfer", "it") == 14d &&
                shortestDistance(document, "Design", "filler" ) == 25d ;
    }


    public static double shortestDistance(String document, String word1, String word2) {
       String arr[] = document.split(" ");
        int m=-1;
        int n=-1;

        int min = Integer.MAX_VALUE;

        for(int i=0; i<arr.length; i++){
            String s = arr[i];
            if(word1.equals(s)){
                m = i;
                if(n!=-1)
                    min = Math.min(min, m-n);
            }else if(word2.equals(s)){
                n = i;
                if(m!=-1)
                    min = Math.min(min, n-m);
            }
        }

        return min;
    }
}
