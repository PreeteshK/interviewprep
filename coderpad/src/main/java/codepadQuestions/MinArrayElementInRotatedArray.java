package codepadQuestions;

public class MinArrayElementInRotatedArray {

    public static void main(String[] args) {

        int arr1[] =  {5, 6, 2, 3, 4};
        int n1 = arr1.length;

        System.out.println("The minimum element is "+ findMin(arr1));
    }

    public static int findMin(int []arr){

             int low = 0, high = arr.length-1;
             int min = -1;

             while (low<=high){
                 int mid = (low + high)/2;
                 if(mid == high){
                     min = arr[mid];
                     break;
                 }

                 if(arr[mid] < arr[high]){

                     high = mid;
                 }else{
                     low = mid+1;
                 }
             }
return min;
    }
}
