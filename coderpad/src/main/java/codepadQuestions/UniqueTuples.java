package codepadQuestions;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class UniqueTuples {


    public static void main(String[] args) {


    String input = "aab";
    String input1 = "abbccde";

    HashSet<String> result = uniqueTuples(input, 2);
    HashSet<String> result1 = uniqueTuples(input1, 2);

		if ((result.contains("aa") && result.contains("ab"))
                && (result1.containsAll(Arrays.asList("ab", "bb", "bc", "cc", "cd", "de")) && result1.size() == 6)) {
        System.out.println("Test passed.");
    } else {
        System.out.println("Test failed.");
    }

}

public static HashSet<String> uniqueTuples(String input,int len) throws IllegalArgumentException{

    int inputLength =0;
    HashSet<String>tuppleSet = new HashSet<String>();
        if(Objects.isNull(input)) throw new IllegalArgumentException("Wrong input");
        else
             inputLength = input.length();

    if(len<=0 || len ==0 || len>inputLength) throw new IllegalArgumentException("Wrong tuple length");

    for (int i = 0; i < (inputLength-len+1) ; i++) {
          tuppleSet.add(input.substring(i,(i+len)));
    }
    return tuppleSet;
    }

}
