package codepadQuestions;

public class PascalTriangle {

    public static void main(String[] args) {
        if (pascal(0, 0) == 1 && pascal(1, 2) == 2
                && pascal(5, 6) == 6 && pascal(4, 8) == 70
                && pascal(6, 6) == 1) {
            System.out.println("Pass");
        } else {
            System.out.println("Failed");
        }
    }


    public static int pascal(int col, int row) {
        int[][] pascalArray = new int[row + 1][row + 1];
        for (int rows = 0; rows <= row; rows++) {
            for (int cols = 0; cols <= rows; cols++) {
                if (cols == 0 || cols == rows) {
                    pascalArray[rows][cols] = 1;
                } else {
                    pascalArray[rows][cols] =
                            pascalArray[rows - 1][cols - 1] + pascalArray[rows - 1][cols];
                }
            }
        }
        return pascalArray[row][col];
    }

}
