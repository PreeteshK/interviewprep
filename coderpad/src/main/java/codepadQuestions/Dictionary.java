package codepadQuestions;

import java.util.Arrays;

public class Dictionary {

    private String[] entries;

    public Dictionary(String[] entries) {
        this.entries = entries;
    }

    public boolean contains(String word){
        return Arrays.asList(entries).contains(word);
    }
}
