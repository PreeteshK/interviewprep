package codepadQuestions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrimeFactorization {

    public static void main(String[] args) {
      //  System.out.println(primeFactorization(2) + " " + primeFactorization(259));
        if(primeFactorization(24).equals(Arrays.asList(2,2,2,3)) && primeFactorization(30).equals(Arrays.asList(2,3,5))) {
            System.out.println("All passed");
        }else {
            System.out.println("Failed");
        }

       // primeFactors(259);
    }

    public static List<Integer> primeFactorization(int n)
    {
        // Print the number of 2s that divide n

        List<Integer> list = new ArrayList<>();
        while (n%2==0)
        {
            list.add(2);
       //     System.out.print(2 + " ");
            n /= 2;
        }

        // n must be odd at this point.  So we can
        // skip one element (Note i = i +2)
        for (int i = 3; i <= Math.sqrt(n); i+= 2)
        {
            // While i divides n, print i and divide n
            while (n%i == 0)
            {
                list.add(i);
                System.out.print(i + " ");
                n /= i;
            }
        }

        // This condition is to handle the case whien
        // n is a prime number greater than 2
        if (n > 2)list.add(n);
           // System.out.print(n);

        return list;
    }


/*    public static List<Integer> primeFactorization(int n){


        List<Integer>factors = new ArrayList<>();
        if(n<2)
            return factors;

        for (int i = 2; i <= n; i++) {
                    while (n%i==0){
                        n=n/i;
                        System.out.println("++" + i);
                        factors.add(i);
                    }
        }
        return factors;
    }*/
}
