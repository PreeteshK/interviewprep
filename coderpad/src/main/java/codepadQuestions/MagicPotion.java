package codepadQuestions;

public class MagicPotion {

    public static void main(String[] args) {

        if (minimalSteps("ABCDABCEFGH") == 11 && minimalSteps("ABCABCE") == 5 && minimalSteps("ABABCABABCE")==6) {
            System.out.println("Pass");
        } else {
            System.out.println("Fail");
        }
    }


    private static int minimalSteps(String inp) {
        String ans = "";
        String temp = "";
        int i=0;
        while (i<inp.length()){
            if (temp.length()>0 && (i+temp.length() < inp.length()) && inp.substring(i, i+temp.length()).equals(temp)){
                ans = ans+ "*";
                i = i+temp.length();
                temp = temp+temp;
            } else {
                ans = ans+inp.charAt(i);
                temp = temp +inp.charAt(i);
                i++;
            }
        }
        System.out.println(ans);
        return ans.length();
    }

}
