package codepadQuestions;

public class CountCyclesInArray {

    public static int countLengthOfCycle( int[] arr, int startIndex) {
        //your code goes here
        int[] counters = new int[arr.length];
        int count = 0;
        while (counters[startIndex] == 0){
            counters[startIndex] = count++;
            startIndex = arr[startIndex];
        }
        return count - counters[startIndex];
    }

    public static void main( String[] args ) {

    boolean testsPassed = true;
    testsPassed &= countLengthOfCycle(new int[]{1, 0}, 0) == 2;
    testsPassed &= countLengthOfCycle(new int[]{1, 2, 0}, 0) == 3;
    testsPassed &= countLengthOfCycle(new int[] { 1, 3, 0, 4, 1 }, 0) == 3;
    testsPassed &= countLengthOfCycle(new int[] { 1, 3, 0, 4, 3 }, 0) == 2;
        if(testsPassed) {
        System.out.println( "Test passed." );
    } else {
        System.out.println( "Test failed." );
    }
}

}
