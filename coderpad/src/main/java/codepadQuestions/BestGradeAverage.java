package codepadQuestions;

import java.util.*;

public class BestGradeAverage {

    public static int bestAvgGrade(String [][]scores){

        if(scores.length <1)return 0;
        Map<String, ArrayList<Integer>> studentsToScoreMap = new HashMap<String, ArrayList<Integer>>();

       for (String[] scoreRow : scores){

           if(scoreRow.length <2)return 0;

           String student = scoreRow[0];
           Integer score = Integer.parseInt(scoreRow[1]);


           ArrayList<Integer>scoreList =studentsToScoreMap.get(student);

           if(Objects.isNull(scoreList)){
               scoreList = new ArrayList<Integer>();
               scoreList.add(score);
               studentsToScoreMap.put(student,scoreList);
           }else
               scoreList.add(score);
       }

        Double max = -Double.MAX_VALUE;

       for(ArrayList<Integer> studentScore : studentsToScoreMap.values()){
           Integer sum = 0;

           for(Integer number: studentScore){
               sum = sum+number;
           }
           Double average = sum/(double)studentScore.size();

max = Math.max(max,average);

       }
return (int)Math.floor(max);
    }

    public static boolean pass()
    {
        String[][] s1 = { { "Rohan", "81" },
                { "Sachin", "102" },
                { "Ishan", "55" },
                { "Sachin", "18" } };

        return bestAvgGrade(s1) == 81;
    }

    public static void main(String[] args)
    {
        if(pass())
        {
            System.out.println("Pass");
        }
        else
        {
            System.out.println("Some Fail");
        }
    }
}
