package codepadQuestions;

public class AtoiProblem {


    public static void main(String[] args) {
        if(doTestsPass()) {
            System.out.println("All tests passed ...");
        } else {
            System.out.println("There are test failures");
        }
    }

    public static boolean doTestsPass() {
        boolean result = true;
        result = result && atoi("0") == 0;
        result = result && atoi("1") == 1;
        result = result && atoi("123") == 123;
        result = result && atoi("-1") == -1;
        result = result && atoi("-123") == -123;
        result = result && atoi("123a") == 123;
        result = result && atoi("a123") == 0;

        String intMax = String.valueOf(Integer.MAX_VALUE);
        result = result && atoi(intMax) == Integer.MAX_VALUE;

        String intMin = String.valueOf(Integer.MIN_VALUE);
        result = result && atoi(intMin) == Integer.MIN_VALUE;

        return result;
    }

    public static int atoi(String str){

        if(str.length()<1)return 0;

        char flag = '+';
        int i = 0;

        str = str.trim();// takes care of white spaces

        if(str.charAt(i) == '-'){
            flag = '-';
            i++;
        }else if(str.charAt(i) == '+'){
            i++;
        }
double result = 0;
        while (str.length()>i && str.charAt(i) > '0' && str.charAt(i) < '9'){

            result = result*10 + (str.charAt(i)-'0');
            i++;
        }

        if(flag == '-')result = -result;

        if(result > Integer.MAX_VALUE)result =Integer.MAX_VALUE;
        if(result < Integer.MIN_VALUE)result =Integer.MIN_VALUE;

        return (int)result;

    }

}
