package codepadQuestions;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GroupAnagram {

    public static void main(String[] args) {
        String input = "cat dog tac sat tas god dog";
        setOfAnagrams(input);
    }

    static void setOfAnagrams(String inputString){
/*        String [] inputArray = inputString.split(" ");
        Map<String, List> map = new LinkedHashMap();
        Stream.of(inputArray).forEach(l -> {
            char[] charsArray = l.toCharArray();
            Arrays.sort(charsArray);
            String s = String.valueOf(charsArray);
            if (map.containsKey(s)){
                map.get(s).add(l);
            } else {
                List list = new ArrayList();
                list.add(l);
                map.put(s, list);
            }
        });
        List list1=(List)map.values().stream().
                flatMap(list->list.stream().map(l1->l1)).
                collect(Collectors.toList());
        System.out.println(list1.stream().reduce((x1,x2)->x1+" "+x2).get());*/

       String []inputArray = inputString.split(" ");
       Map<String,List> listMap = new LinkedHashMap<String,List>();
       Stream.of(inputArray).forEach(l -> {
           char[] charArray = l.toCharArray();
           Arrays.sort(charArray);
           String s  = String.valueOf(charArray);
           if(listMap.containsKey(s)){
               listMap.get(s).add(l);
           }else{
               List list = new ArrayList();
               list.add(l);
               listMap.put(s,list);
           }
       });
      //  System.out.println(listMap);

       List list1 = (List) listMap.values().stream().flatMap(list -> list.stream()).map(l1 -> l1).collect(Collectors.toList());
        System.out.println(list1.stream().reduce((x1,x2)->x1+" "+x2).get());

    }



}
