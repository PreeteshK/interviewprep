package sorting;

public class SelectionSort {

    public static void main(String[] args) {

        int arr[] = {5,8,10,2,40,3,7,19,16};

        for (int i = 0; i <arr.length ; i++) {
            int minIndex = i;
            for (int j = i+1; j < arr.length; j++) {
                    if (arr[j]<arr[minIndex])
                        minIndex = j;
            }

            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }

        for (int k = 0; k < arr.length; k++) {
            System.out.print(arr[k] + " ");
        }
    }
}
