package sorting;

public class BubbleSort {

    public static void main(String[] args) {

        int arr[] = {5,8,10,2,40,3,7,19,16};
        doBubbleSort(arr);
    }

    static void doBubbleSort(int []arr){

        int length = arr.length;
        int temp = 0;

        for (int i = 0; i <length-1 ; i++) {
            for (int j=0; j < length-i-1; j++) {

                if(arr[j] > arr[j+1]){
                  temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
            System.out.println();
            printArray(arr);

        }

        System.out.println("Sorted array : ");
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        for (int k = 0; k < arr.length; k++) {
            System.out.print(arr[k] + " ");
        }
        
    }
}
