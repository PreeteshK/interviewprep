package java8features;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class TestStream {

    public static void main(String[] args) {


        List<Integer> number = Arrays.asList(12,13,24,35);

        List<String> words = Arrays.asList("GFG", "Geeks", "for",
                "GeeksQuiz", "GeeksforGeeks");
        Optional<String> longestString = words.stream()
                .reduce((word1, word2)
                        -> word1.length() < word2.length()
                        ? word1 : word2);
      //  longestString.ifPresent(System.out::println);

        Optional<Integer> sum = number.stream().filter(x->x>10 && x <20).reduce((x1,x2) -> (x1+x2));
        sum.ifPresent(System.out::println);

     //   Optional<Integer> sumArray = number.stream().reduce(x)

    //    int even = number.stream().filter(x->x %2==0).reduce(0,(ans,i)-> ans+i);
      //  System.out.println("The number is : " + even);

        List<User> users = Arrays.asList(new User("John", 30), new User("Julie", 35));
        int computedAges =
                users.parallelStream().reduce(0, (partialAgeResult, user) -> partialAgeResult + user.getAge(),Integer::sum);
        System.out.println(computedAges);

    }
}
