package multitherading.synchronization;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class Processor1 implements Runnable{

    private int id;

    public Processor1(int id) {
        this.id = id;
    }

    public void run(){
        System.out.println("Starting task: " + id);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {

        }

        System.out.println("Completed task: " + id);
    }
}
public class ThreadPool {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2);

        for (int i = 0; i <5 ; i++) {

            executor.submit(new Processor1(i));
        }

        executor.shutdown();
        System.out.println("All tasks submitted");

        try {
            executor.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
        }

        System.out.println("All tasks completed");
    }
}
