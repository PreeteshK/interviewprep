package multitherading.synchronization;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Processor6{
    private int count = 0;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void increament(){
        for (int i = 0; i < 10000; i++) {
            count++;
        }
    }


    public void firstThread() throws InterruptedException{
        lock.lock();
        condition.await();
        try {
            increament();
        }finally {
            lock.unlock();
        }
    }

    public void secondThread() throws InterruptedException{

        Thread.sleep(1000);
        lock.lock();
        condition.signal();
        try {
            increament();
        }finally {
            lock.unlock();
        }

       // System.out.println("count is " +count);

    }

    public void finish(){

        System.out.println("count is " +count);
    }
}



public class ReentrantLocks {

    public static void main(String[] args) throws InterruptedException {

        Processor6 fir = new Processor6();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    fir.firstThread();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    fir.secondThread();
                } catch (InterruptedException e) {

                }
            }
        });

        t1.start();
        t2.start();

        t1.join();
       t2.join();

        fir.finish();


    }
}
