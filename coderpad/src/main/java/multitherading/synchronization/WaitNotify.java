package multitherading.synchronization;


import java.util.Scanner;

class Processor4 {

    public void producer() throws InterruptedException {

        synchronized (this){
            System.out.println("Started running the process ..");
            wait();
            System.out.println("Resumed ..");

        }
    }

    public void consumer() throws InterruptedException {

        Scanner scanner = new Scanner(System.in);
        Thread.sleep(1000);
        synchronized (this){
            System.out.println("Press return to continue ... ");
            scanner.nextLine();
            System.out.println("Return key pressed");
            notify();
        }
    }

}
public class WaitNotify {


    public static void main(String[] args) {
        Processor4 processor4 = new Processor4();
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor4.producer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    processor4.consumer();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();


    }

}
