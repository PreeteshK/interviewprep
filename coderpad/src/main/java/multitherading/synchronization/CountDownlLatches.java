package multitherading.synchronization;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CountDownlLatches {


    public static void main(String[] args) {

        CountDownLatch latch = new CountDownLatch(3);
        ExecutorService executors = Executors.newFixedThreadPool(3);

        for (int i = 0; i <5; i++) {
            executors.submit(new Processor3(latch));
        }
        try {
            latch.await(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
        }
        System.out.println("Completed." + System.currentTimeMillis());
    }
}

class Processor3 implements Runnable {

    private CountDownLatch latch;

    public Processor3(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        System.out.println("Started .."+ System.currentTimeMillis() );

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {

        }
        latch.countDown();
    }
}

